import React from 'react';
import { Graph } from "react-d3-graph";
import { select as d3Select, selectAll as d3SelectAll, event as d3Event } from "d3-selection";

const allNodes = d3.selectAll(".node");

// graph payload (with minimalist structure)
const data = {
    nodes: [{ id: "Harry" }, { id: "Sally" }, { id: "Alice" }],
    links: [{ source: "Harry", target: "Sally" }, { source: "Harry", target: "Alice" }],
};

// the graph configuration, you only need to pass down properties
// that you want to override, otherwise default ones will be used
const myConfig = {
    nodeHighlightBehavior: true,
    node: {
        color: "lightgreen",
        size: 120,
        highlightStrokeColor: "blue",
    },
    link: {
        highlightColor: "lightblue",
    },
};

// graph event callbacks
const onClickNode = function (nodeId) {
    window.alert(`Clicked node ${nodeId}`);
};

function GraphD3() {
    return (
        <Graph
            id="graph-id" // id is mandatory, if no id is defined rd3g will throw an error
            data={data}
            config={myConfig}
            onClickNode={onClickNode}
        />
    );
}

export default GraphD3;