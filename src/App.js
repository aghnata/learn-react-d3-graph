import React from 'react';
import './App.css';
import GraphD3 from './GraphD3';

function App() {
  return (
    <div className="App">
      <h1>Hi I'm using react-d3-graph</h1>
      <h3>there is my graph: </h3>
      <GraphD3/>
    </div>
  );
}

export default App;
